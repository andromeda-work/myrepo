import { FacilityComponent } from './facility/facility.component';
import { EntityComponent } from './entity/entity.component';
import { InfoComponent } from './info/info.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'info',component:InfoComponent} , 
  {path:'login',component:LoginComponent},
  {path:'entity',component:EntityComponent},
  {path:'facility',component:FacilityComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
