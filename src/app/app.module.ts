import { SharedService } from './shared.service';
import { InfoComponent } from './info/info.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FacilityComponent } from './facility/facility.component';
import { ShowFacilityComponent } from './facility/show-facility/show-facility.component';
import { AddEditFacilityComponent } from './facility/add-edit-facility/add-edit-facility.component';
import { EntityComponent } from './entity/entity.component';
import { ShowEntityComponent } from './entity/show-entity/show-entity.component';
import { AddEditEntityComponent } from './entity/add-edit-entity/add-edit-entity.component';
import {HttpClientModule} from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { NavigationAfterloginComponent } from './navigation/navigation-afterlogin/navigation-afterlogin.component';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InfoComponent,
    EntityComponent,
    ShowEntityComponent,
    AddEditEntityComponent,
    FacilityComponent,
    ShowFacilityComponent,
    AddEditFacilityComponent,
    NavigationComponent,
    NavigationAfterloginComponent    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    ToastrModule.forRoot({positionClass:'toast-center-center'}),
    BrowserAnimationsModule,
    DataTablesModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
