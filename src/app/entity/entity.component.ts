import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-entity',
  templateUrl: './entity.component.html',
  styleUrls: ['./entity.component.css']
})
export class EntityComponent implements OnInit {

  constructor(private toast:ToastrService) { }

  ngOnInit(): void {
  }
  
  logout()
  {
    this.toast.show("Logged Out!");
  }
 
}
