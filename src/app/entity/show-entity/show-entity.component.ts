import { SharedService } from './../../shared.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-entity',
  templateUrl: './show-entity.component.html',
  styleUrls: ['./show-entity.component.css']
})
export class ShowEntityComponent implements OnInit {

  constructor(private service:SharedService) { }

  EntityList: any=[];

  ngOnInit(): void {
    this.refreshEntityList();
  }

  refreshEntityList(){
    this.service.getEntity().subscribe(data=>
      {this.EntityList=data;})
  }

}
