import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationAfterloginComponent } from './navigation-afterlogin.component';

describe('NavigationAfterloginComponent', () => {
  let component: NavigationAfterloginComponent;
  let fixture: ComponentFixture<NavigationAfterloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavigationAfterloginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationAfterloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
