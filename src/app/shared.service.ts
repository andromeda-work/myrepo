import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  // LogUser(val: { UserID: string | undefined; Password: string | undefined; }) {
  //   throw new Error('Method not implemented.');
  // }

readonly APIUrl="http://localhost:49842/api"
/* readonly PhotoUrl="http://localhost:65041/Photos"
readonly APIUrl1="http://localhost:54847"   */

constructor(private http:HttpClient) { }


  getEntity():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/entitymaster');
  }
  
   loginApi(val:any){
     return this.http.post(this.APIUrl+`/login`,val);
   }

  addEntity(val:any){
    return this.http.post(this.APIUrl+'/entitymaster',val);
  }

  updateEntity(val:any){
    return this.http.put(this.APIUrl+'/entitymaster',val) ;
  }

  delete(val:any){
    return this.http.delete(this.APIUrl+'/entitymaster',val);
  }

 /*  getEmpList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/Employee');
  }

  addEmployee(val:any){
    return this.http.post(this.APIUrl+'/Employee',val);
  }
  updateEmployee(val:any){
    return this.http.put(this.APIUrl+'/Employee',val);  
  }
  deleteEmployee(val:any){
    return this.http.delete(this.APIUrl+'/Employee/',val);
  }

  uploadPhoto(val:any){
    return this.http.post(this.APIUrl+'/Employee/SaveFile',val);
  } 

  getAllDepartmentNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/Employee/GetAllDepartmentNames');
  } */
}
  